#ifndef WORKER_H
#define WORKER_H

#include <QObject>
#include <QNetworkRequest>
#include <QApplication>
#include <QNetworkAccessManager>
#include <QByteArray>
#include <iostream>
#include <QNetworkReply>
#include <QDebug>
#include <QSslConfiguration>
#include <QtWebKit>
#include "webview.h"

 #include <QStack>



class MyWebPage : public QWebPage
{
    Q_OBJECT
signals:
    void openPage(MyWebPage*);

protected:
void javaScriptAlert(QWebFrame *, const QString &st)
{
    //qDebug()<<"javaScriptAlert: " + st;
}
bool javaScriptConfirm ( QWebFrame, const QString & msg )
{
    //qDebug()<<"javaScriptConfirm: " + msg;
    return true;
}

void javaScriptConsoleMessage( const QString & msg, int lineNumber, const QString & sourceID )
{
    //qDebug()<<"javaScriptConsoleMessage: " + msg + " line " + QString::number(lineNumber) + " sourceID " + sourceID;
}

bool javaScriptPrompt ( QWebFrame * frame, const QString & msg, const QString & defaultValue, QString * result )
{
    //qDebug()<<"javaScriptPrompt: " + msg;
    return QWebPage::javaScriptPrompt(frame, msg, defaultValue, result);
}

QWebPage* createWindow ( WebWindowType  )
{
    MyWebPage* p = new MyWebPage();
    p->setNetworkAccessManager(networkAccessManager());
    emit openPage(p);

    return p;
}

};

class Worker : public QObject
{
	Q_OBJECT

public:
	Worker(QObject *parent);
	~Worker();
	
public slots:
		void command(const QString&);
		void redirect(const QUrl & url);
        void printImage();
        void addPage(MyWebPage* p);

        void linkClickedSlot( QUrl url );
signals:
		void next();
		void repeat();

private:
    QStack<MyWebPage*> pageStack;
	QWebInspector* inspec;
    QStack<WebView *> viewStack;
};

#endif // WORKER_H
