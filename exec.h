#ifndef EXEC_H
#define EXEC_H

#include <QThread>
#include "activity.h"

class Exec : public QThread
{
	Q_OBJECT

public:
	Exec(QObject *parent);
	~Exec();

protected:
     void run()
     {
		 for(int i = 0; i<=12; i++){
			new Activity(0);
		 }
	
         exec();
     }

private:
	
};

#endif // EXEC_H
