#-------------------------------------------------
#
# Project created by QtCreator 2013-05-09T23:34:33
#
#-------------------------------------------------

QT       += core webkit network gui

TARGET = ExJSqt2
#CONFIG   += console
#CONFIG   -= app_bundle

TEMPLATE = app


SOURCES += main.cpp \
    worker.cpp \
    storage.cpp \
    reader.cpp \
    filereader.cpp \
    exec.cpp \
    activity.cpp \
    settings.cpp \
    webview.cpp

HEADERS += \
    worker.h \
    storage.h \
    reader.h \
    filereader.h \
    exec.h \
    activity.h \
    settings.h \
    webview.h
