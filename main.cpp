
#include <iostream>

#include "worker.h"
#include "reader.h"
#include "qobject.h"
#include "filereader.h"
#include "storage.h"
#include "activity.h"
#include "exec.h"
#include "QDebug"
#include "settings.h"


Settings settings;
Storage storage(0);
int main(int argc, char *argv[])
{
	QApplication a(argc, argv);

    for (int i = 0; i < argc; i++)
    {
        QString param(argv[i]);

        if(param.startsWith("activity="))
        {
            param.remove(QRegExp("activity="));
            settings.setActivityCount(param.toInt());
        }
        else if(param.startsWith("show=")){
            param.remove(QRegExp("show="));
            settings.setShowable(param == "true");
        }
        else if(param.startsWith("turnOffAtEnd=")){
            param.remove(QRegExp("turnOffAtEnd="));
            settings.setTurnOff(param == "true");
        }
        else if(param.startsWith("file=")){
            param.remove(QRegExp("file="));
            settings.setFile(param);
            qDebug()<<"file is"<<param;
        }
    }
    storage.load();


/*	Worker w(0);
	FileReader r(0);
	
	QObject::connect(&r, SIGNAL(read(const QString&)), &w, SLOT(command(const QString&)));
	QObject::connect(&w, SIGNAL(next()), &r, SLOT(waitForDone()));

	w.command("help");

	r.start();*/
	QVector<Activity*> vector;

    for(int i = 0; i < settings.getActivityCount(); i++){
        vector.append(new Activity(0));
	}


	return a.exec();
}

