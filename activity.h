#ifndef ACTIVITY_H
#define ACTIVITY_H

#include <QThread>

#include "worker.h"
#include "filereader.h"



class Activity : public QObject
{
	Q_OBJECT

public:
	Activity(QObject *parent);
	~Activity();

private:
	Worker w;
	FileReader r;
};

#endif // ACTIVITY_H
