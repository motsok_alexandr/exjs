#include "filereader.h"
#include "qfile.h"
#include "qapplication.h"
#include "qdebug.h"



FileReader::FileReader(QObject *parent)
	: QObject(parent)
{
	 connect(this, SIGNAL(repeatCurrent()), this, SLOT(waitForDone()));
	 //connect(this, SIGNAL(repeat()), this, SLOT(failVerification()));
	 state = Work;

     verificationTimer = startTimer(2 * 1000 * 60);
	 repeatFlag = 0;
	 timer = -1;

     allCurrentCommands = storage.getCommands();
	 currentCommands = allCurrentCommands;
}

FileReader::~FileReader()
{
    qDebug()<<"in FileReader destruct";
}

void FileReader::start()
{
	waitForDone();
}


void FileReader::failVerification()
{
	qDebug()<<"!!!!!!!!!!!!!!!!!!!!!!!! failVerification!!!!!!!!!!!!!!!!!!!!!!!!";
	currentCommands = allCurrentCommands;
	killTimer(verificationTimer);
    verificationTimer = startTimer(2 * 1000 * 60);

	if(timer!=-1){
		killTimer(timer);
		timer =-1;
	}

	state = Work;
	
	emit repeatCurrent();
}


void FileReader::waitForDone()
{
	if(state == Wait){
        qDebug()<<"------> unplaned case";
		return;
	}
	else if(state == WaitForDone){
		qDebug()<<"ggg";
		state = Work;
		if(timerWaitForDone !=-1){
            killTimer(timerWaitForDone);
			timerWaitForDone = -1;
		}
	}
	else if(state == WaitUntillUpload){
		state = Work;
		if(timerUpload!=-1){
			killTimer(timerUpload);
			timerUpload = -1;
		}
		state = Wait;
        timer = startTimer(3000);
		return;
	}
	

	while(1){

		if(currentCommands.isEmpty()){
            allCurrentCommands = storage.getCommands();
			currentCommands = allCurrentCommands;
			if(currentCommands.size() == 0){
				    killTimer(verificationTimer);
				    parent()->deleteLater();
				    qDebug()<<"I have finished! ;)";
					return;
			}
		}
		QString a = currentCommands.takeFirst();
		a.remove("\n");
        //qDebug()<<a;

		if(a == "reset" )
		{
			killTimer(verificationTimer);
            verificationTimer = startTimer(2 * 1000 * 60);
			repeatFlag = 0;
		}

		if(a.startsWith("verification@"))
		{
            killTimer(verificationTimer);
            verificationTimer = startTimer(2 * 1000 * 60);
			emit read(a);
			break;
		}
		
		if(a == "done"){
			break;
		}
		if(a == "wait"){
			state = Wait;
			timer = startTimer(5000);
			break;
		}
        if(a == "wait1"){
            state = Wait;
            timer = startTimer(2000);
            break;
        }
		if(a == "waitForDone"){
		qDebug()<<"--waitfordone"<<this;
			state = WaitForDone;
			timerWaitForDone = startTimer(20000);
			return;
		}
		if(a.startsWith("waitUntillUpload")){
		    buffer = a;
		    state = WaitUntillUpload;
		    timerUpload = startTimer(1 * 1000);
		    emit read(a);
		    return;
		}
		else{
			emit read(a);
		}
	}
}

 void FileReader::timerEvent(QTimerEvent *event)
 {
	 if(event->timerId() == timer){
		killTimer(timer);
		timer = -1;
		state = Work;
	 }
	 else if(event->timerId() == timerWaitForDone){
		killTimer(timerWaitForDone);
		timerWaitForDone = -1;
	 }
	 else if(event->timerId() == verificationTimer){
        emit  printFail();
		if(timer > 0){
		    killTimer(timer);
		    timer = -1;
		}
		if(timerUpload > 0){
		    killTimer(timerUpload);
		    timerUpload = -1;
		}
		if(timerWaitForDone > 0){
		    killTimer(timerWaitForDone);
		    timerWaitForDone = -1;
		}
		 currentCommands = allCurrentCommands;
		 state = Work;
         qDebug()<<"--->repeat by timeout!\n"<<currentCommands;
	 }
	 else if(event->timerId() == timerUpload){
		emit read(buffer);
		return;
	 }

	 emit repeatCurrent();
 }
 
 
