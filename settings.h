#ifndef SETTINGS_H
#define SETTINGS_H

#include <QString>

class Settings
{
public:
    Settings();

    void setActivityCount(const int& a)
    {
        activityCount = a;
    }
    int getActivityCount() const
    {
        return activityCount;
    }
    void setShowable(const bool& s)
    {
        showable = s;
    }
    bool isShowable() const
    {
        return showable;
    }
    void setFile(const QString& f)
    {
        file = f;
    }
    QString getFile() const
    {
        return file;
    }
    void setTurnOff(bool t){
        turnOf = t;
    }
    bool getTurnOf() const
    {
        return turnOf;
    }

private:
    int activityCount;
    bool showable;
    bool turnOf;
    QString file;


};

#endif // SETTINGS_H


