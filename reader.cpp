#include "reader.h"
#include <iostream>
#include <string>


using namespace std;
Reader::Reader()
	: QThread(0)
{
}

Reader::~Reader()
{
}


void Reader::run()
{
	while(1){
		std::string st;
		getline(cin, st);

		QString cmd;
		cmd = QString::fromStdString(st);
		emit read(cmd);
	}
}