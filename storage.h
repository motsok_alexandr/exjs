#ifndef STORAGE_H
#define STORAGE_H

#include <QObject>
#include <qmutex.h>
#include <qstringlist.h>

class Storage : public QObject
{
	Q_OBJECT

public:
	Storage(QObject *parent);
	~Storage();

	QStringList getCommands();
    void load();

private:
	QList<QStringList> commands;
    //QMutex mutex;
};



#endif // STORAGE_H
