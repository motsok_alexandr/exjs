#ifndef FILEREADER_H
#define FILEREADER_H

#include <QObject>
#include "qstringlist.h"
#include "storage.h"

extern Storage storage;


class FileReader : public QObject
{
	Q_OBJECT

public:
	enum State{Wait, Work, WaitForDone, WaitUntillUpload};
	FileReader(QObject *parent);
	~FileReader();

public slots:
	void waitForDone();
	void start();
	void failVerification();

protected:
	void timerEvent(QTimerEvent *event);
	
signals:
	void read(const QString&);
	void repeatCurrent();
    void printFail();

private:
	QList<QStringList> commands;
	QStringList currentCommands;
	QStringList allCurrentCommands;

	int timer;
	int timerWaitForDone;
	int verificationTimer;
	int timerUpload;
	State state;
	int repeatFlag;
	QString buffer;

};

#endif // FILEREADER_H
