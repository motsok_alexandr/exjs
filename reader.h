#ifndef READER_H
#define READER_H

#include <QThread>
#include <iostream>

class Reader : public QThread
{
	Q_OBJECT

public:
	Reader();
	~Reader();
signals:
	void read(const QString&);

protected:
		void run();

private:
	
};

#endif // READER_H
