#include "worker.h"
#include "qfile.h"
#include "QWebInspector"
#include "settings.h"

extern Settings settings;

Worker::Worker(QObject *parent)
    : QObject(parent), inspec(0)
{
	QWebSettings * sett = QWebSettings::globalSettings();
	sett->setAttribute(QWebSettings::DeveloperExtrasEnabled, true);
    sett->setAttribute(QWebSettings::JavascriptCanOpenWindows, true);

    MyWebPage* page = new MyWebPage();
    pageStack.push(page);
    page->setLinkDelegationPolicy(QWebPage::DelegateAllLinks);

    connect(page, SIGNAL(openPage(MyWebPage*)), this, SLOT(addPage(MyWebPage*)));
}


void Worker::addPage(MyWebPage* p)
{
    pageStack.append(p);

    if(settings.isShowable()){
        WebView* v = new WebView();
        v->setPage(p);
        v->show();
        viewStack.push(v);
    }
}


Worker::~Worker()
{
    while(!pageStack.empty()){
        pageStack.pop()->deleteLater();
    }

    while(!viewStack.empty()){
        viewStack.pop()->deleteLater();
    }

	if(inspec != 0){
		delete inspec;
	}

	qDebug()<<"in worker destruct";
}


void Worker::command(const QString& c)
{
	if(c == "wait"){
		return;
	}
	
	if(c == "help"){
		std::cout<<"***************************************************************************\n" \
			   "This program support few commands:\n" \
			   "1. \"site@https://sitename.domain\" for start working with specific site;\n" \
			   "2. \"exec@javascript()\" for script execution;\n" \
			   "3. \"show\" view page and inspector;\n" \
			   "4. \"save@filename\" save html to a file.\n"\
			   "5. \"exit\"\n"
			   "This program was written for better wife in the world. Enjoy, darling!\n"\
			   "***************************************************************************\n\n";
		return;
	}
	if(c == "reset"){
        /*page->disconnect();
		page->deleteLater();
		page = new MyWebPage();
		if(inspec != 0){
			inspec->setPage(page);
			inspec->setVisible(true);
		}

		if(view != 0){
			view->setPage(page);

            connect( view, SIGNAL( linkClicked( QUrl ) ), this, SLOT( linkClickedSlot( QUrl ) ) );
		}
        qDebug()<<"reset";*/
		return;
	}

    if(c == "removeActive"){
        pageStack.pop()->deleteLater();

        if(!viewStack.empty()){
            viewStack.pop()->deleteLater();
        }
        return;
    }


	if(c == "show"){
        if(settings.isShowable()){
         /*   if(inspec == 0){
                inspec = new QWebInspector;
                inspec->setPage(page);
                inspec->setVisible(true);
            }*/

            if(viewStack.empty()){
                WebView* view = new WebView;
                view->setPage(pageStack.top());
                viewStack.push(view);
            }
            viewStack.top()->show();
        }
		return;
	}


	QStringList command = c.split("@");
	if(command.size() != 2){
		std::cout<<"Command is wrong. Error!\n";
		return;
	}

	if(command[0] == "site"){
		QUrl params;
		params.addQueryItem("Accept-Charset", "windows-1251,utf-8;q=0.7,*;q=0.3");
		params.addQueryItem("Accept-Encoding", "gzip,deflate,sdch");
		params.addQueryItem("Accept-Language", "ru-RU,ru;q=0.8,en-US;q=0.6,en;q=0.4");
		params.addQueryItem("Cache-Control", "max-age=0");
		params.addQueryItem("Connection", "keep-alive");
		params.addQueryItem("Host", "www.citimortgage.com");
		params.addQueryItem("User-Agent", "Mozilla/5.0 (Windows NT 6.2; WOW64) AppleWebKit/537.22 (KHTML, like Gecko) Chrome/25.0.1364.152 Safari/537.22");

		QByteArray data;
		data.append(params.toEncoded());
		data.remove(0,1);

		QUrl url(command[1]);
		QNetworkRequest request(url);

        pageStack.top()->mainFrame()->load(request, QNetworkAccessManager::GetOperation, data);
		return;
	}

	if(command[0] == "save"){
		QFile file(command[1]);
		if (!file.open(QIODevice::WriteOnly | QIODevice::Text))
         return;

		QTextStream out(&file);
        out << pageStack.top()->mainFrame()->toHtml();
		out.flush();
		file.flush();
		file.close();
		return;
	}

	if(command[0] == "exec"){
        pageStack.top()->mainFrame()->evaluateJavaScript(command[1]);
		return;
	}
	
	if(command[0] == "waitUntillUpload"){
        if(pageStack.top()->mainFrame()->toHtml().indexOf(command[1]) != -1){
		    emit next();
		}
		return;
	}

	if(command[0] == "verification"){
        if(pageStack.top()->mainFrame()->toHtml().indexOf(command[1]) == -1){
			QFile file("test.txt");
			if (!file.open(QIODevice::WriteOnly | QIODevice::Text))
		         return;

			QTextStream out(&file);
            out << pageStack.top()->mainFrame()->toHtml();
			out.flush();
			file.flush();
			file.close();

			emit repeat();
		}
		else{
			qDebug()<<"Ver ok";
			emit next();
		}

		return;
	}

	std::cout<<command[0].toStdString()<<" is unknown type of command. Error!\n";
}


void Worker::redirect(const QUrl & url)
{
    pageStack.top()->mainFrame()->load(url);
}

void Worker::printImage()
{
    QImage img(pageStack.top()->viewportSize(), QImage::Format_ARGB32);
    QPainter paint(&img);

    pageStack.top()->currentFrame()->render(&paint);
    img.save(QString(QString::number(rand()) + ".png"));
}


void Worker::linkClickedSlot( QUrl url )
    {
    qDebug()<<"******************************";
    //    if (url.is)//isHtml does not exist actually you need to write something like it by yourself
             //view->load(url);
//        else//non html (pdf) pages will be opened with default application
//            QDesktopServices::openUrl( url );
    }
