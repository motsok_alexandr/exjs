#include "activity.h"

Activity::Activity(QObject *parent)
	: QObject(parent), w(this), r(this)
{
	
	QObject::connect(&r, SIGNAL(read(const QString&)), &w, SLOT(command(const QString&)));
	QObject::connect(&w, SIGNAL(next()), &r, SLOT(waitForDone()));

	QObject::connect(&w, SIGNAL(repeat()), &r, SLOT(failVerification()));
    QObject::connect(&r, SIGNAL(printFail()), &w, SLOT(printImage()));

	r.start();
}

Activity::~Activity()
{
	qDebug()<<"---------------------------------->end";
}
