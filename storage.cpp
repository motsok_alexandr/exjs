#include "storage.h"
#include "qfile.h"
#include <qdebug.h>
#include <qapplication.h>
#include "settings.h"



extern Settings settings;

Storage::Storage(QObject *parent)
	: QObject(parent)
{
}


void Storage::load()
{
    QFile file(settings.getFile());
    if (!file.open(QIODevice::ReadOnly | QIODevice::Text))
         return;
    commands.append(QStringList());

    while (!file.atEnd()) {
        QByteArray line = file.readLine();

        commands.last().append(line);

        if(line.indexOf("reset") != -1){
            commands.append(QStringList());
        }
    }
}



Storage::~Storage()
{

}


QStringList Storage::getCommands()
{
	//QMutexLocker locker(&mutex);

	if(commands.empty()){
        settings.setActivityCount(settings.getActivityCount() - 1);
        qDebug()<<"ActivityCount is "<<settings.getActivityCount();
        if(settings.getActivityCount() <= 1){
			qApp->quit();
		}
		return QStringList();
	}
	else {
		qDebug()<<commands.size()<<" to the end.";
		return commands.takeFirst();
	}
}
